class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.find(params[:id])
    @products = Spree::Product.in_taxon(@taxon).includes(master: [:default_price, :images])
    @taxonomies = Spree::Taxonomy.includes(root: :children)
  end
end
