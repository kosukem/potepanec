class Potepan::ProductsController < ApplicationController
  MAX_RELATED_PRODUCTS = 4
  def show
    @product = Spree::Product.find(params[:id])
    @image = @product.images
    @related_products = Spree::Product.related_products(@product).
      includes(master: [:default_price, :images]).
      limit(MAX_RELATED_PRODUCTS)
  end
end
