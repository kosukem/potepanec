require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe 'GET #show' do
    let(:taxon) { create :taxon }

    let!(:products) do
      create_list(:product, 10) do |product|
        product.taxons << taxon
      end
    end

    before do
      get :show, params: { id: taxon.id }
    end

    it 'render show' do
      expect(response).to render_template('show')
    end

    it 'response successflly' do
      expect(response).to be_success
    end

    it 'returns a 200 response' do
      expect(response).to have_http_status '200'
    end

    it 'assigns taxon' do
      expect(assigns(:taxon)).to eq taxon
    end

    it 'assigns taxon products' do
      expect(assigns(:products)).to match_array(taxon.products)
    end
  end
end
