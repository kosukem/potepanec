require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'GET #show' do
    let(:product) { create :product }

    before do
      get :show, params: { id: product.id }
    end

    it 'render show' do
      expect(response).to render_template('show')
    end

    it 'response successflly' do
      expect(response).to be_success
    end

    it 'returns a 200 response' do
      expect(response).to have_http_status '200'
    end
  end

  describe 'related products' do
    let(:taxonomy_categories) { create(:taxonomy, name: 'Categories') }
    let(:mug) { create(:taxon, name: 'Mug', taxonomy: taxonomy_categories) }
    let(:bag) { create(:taxon, name: 'Bag', taxonomy: taxonomy_categories) }
    let(:shirt) { create(:taxon, name: 'Shirt', taxonomy: taxonomy_categories) }

    let!(:mug_products) do
      create_list(:product, 4) do |product|
        product.taxons << mug
      end
    end

    let!(:bag_products) do
      create_list(:product, 5) do |product|
        product.taxons << bag
      end
    end

    let!(:shirt_products) do
      create_list(:product, 6) do |product|
        product.taxons << shirt
      end
    end

    it 'related products include same taxon products' do
      main_mug_product = mug_products.first
      related_products = (mug_products - [main_mug_product])
      get :show, params: { id: main_mug_product.id }
      expect(assigns(:related_products)).to match_array related_products
    end

    it 'related products not include other taxon products' do
      main_bag_product = bag_products.first
      get :show, params: { id: main_bag_product.id }
      expect(assigns(:related_products)).not_to include mug_products.first
    end

    describe 'show number of related products' do
      context 'exist 3 related products' do
        it 'show 3 related products' do
          main_mug_product = mug_products.first
          get :show, params: { id: main_mug_product.id }
          expect(assigns(:related_products).count).to eq 3
        end
      end

      context 'exist 4 related products' do
        it 'show 4 related products' do
          main_bag_product = bag_products.first
          get :show, params: { id: main_bag_product.id }
          expect(assigns(:related_products).count).to eq 4
        end
      end

      context 'exist 5 related products' do
        it 'show 4 related products' do
          main_shirt_product = shirt_products.first
          get :show, params: { id: main_shirt_product.id }
          expect(assigns(:related_products).count).to eq 4
        end
      end
    end
  end
end
